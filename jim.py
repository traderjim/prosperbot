class Solution:
    paths = 0

    def nextStep(self, A, x, y, m, n):

        if A[x][y] == 1:
            return
        if x + 1 < m and A[x + 1][y] == 0:
             self.nextStep(A, x + 1, y, m, n)
        if y + 1 < n and A[x][y + 1] == 0:
             self.nextStep(A, x, y + 1, m, n)

        if x == m - 1 and y == n - 1:
            self.paths += 1
        else:
            pass
        return

    # @param A : list of list of integers
    # @return an integer
    def uniquePathsWithObstacles(self, A):

        # calculate M and N
        # How am I going to track unique paths?

        # [ 0, 0, 0, 0 ]
        # [ 0, 0, 1, 0 ]


        self.paths = 0

        if A == None or len(A[0]) == 0:
            return 0

        n = len(A[0])  ##  2
        m = len(A)  ##   1

        if n == 1 and m == 1 and A[0][0] == 0:
            return 1

        # return A[1][0]
        self.nextStep(A, 0, 0, m, n)
        return self.paths




testA = [[0,0],[0,0],[1,0], [0,0]]
solution = Solution()
print(solution.uniquePathsWithObstacles(testA))



