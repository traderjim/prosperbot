import prosperAuth
import requests
import json
import logging
import time
import traceback
import datetime
from datetime import datetime, timedelta

properbaseurl = "https://api.prosper.com"

last_stat_check = float('0')


## There are Active Listings and Active Loan Numbers
class Payment:
    def __init__(self, loanNumber: int, listingId: int, isActive: bool = False, payment: float = 0.0,
                 yeild: float = 0.0):
        self.loanNumber = loanNumber
        self.listingId = listingId
        self.isActive = isActive
        self.payment = payment
        self.yeild = yeild

    def __str__(self) -> str:
        return str(self.loanNumber) + ", " + str(self.listingId) + ", " + str(self.isActive) + ", " + str(
            self.payment) + ", " + str(self.yeild)

    def setPayment(self, payment: float):
        self.payment = payment

    def setYeild(self, yeild: float):
        self.yeild = yeild


class NoteStats:
    def __init__(self):
        self.payments = []
        self.pending_listings = set()  ## This is ONLY listings that have been invested this iteration of investments
        self.total_yeild = 0

    def __str__(self):
        msg = "Total Notes: " + str(self.getTotalActiveNotes()) + "\nPayments Collected: $" + str(
            self.getMTDPaymentsDeposited()) + "\nAverage Yeild: " + str(self.getAverageYeild()) + "%"
        msg += "\n\n Loan Number, Listing ID, Active, MTD Payments, Yeild \n"
        for pmt in self.payments:
            msg += str(pmt) + "\n"
        return msg

    def addPayment(self, payment: Payment):
        self.payments.append(payment)

    def getPaymentForListingId(self, listingId: int) -> Payment:
        for payment in self.payments:
            if payment.listingId == listingId:
                return payment
        return None

    def getPaymentForLoanNumber(self, loanNumber: int) -> Payment:
        for payment in self.payments:
            if payment.loanNumber == loanNumber:
                return payment
        return None

    def addPaymentForLoanNumber(self, loanNumber: int, payment: float):
        pmt = self.getPaymentForLoanNumber(loanNumber)
        if pmt != None:
            pmt.payment += payment

    def setYeildForLoanNumber(self, loanNumber: int, yeild: float):
        pmt = self.getPaymentForLoanNumber(loanNumber)
        if pmt != None:
            pmt.yeild = yeild
            self.total_yeild += yeild

    def getLoanNumbers(self, ActiveOnly: bool = True):
        listings = []
        for payment in self.payments:
            if ActiveOnly:
                if payment.isActive:
                    listings.append(payment.loanNumber)
            else:
                listings.append(payment.loanNumber)
        return listings

    def getAverageYeild(self) -> float:
        return round(self.total_yeild / self.getTotalActiveNotes() * 100, 2)

    def getTotalActiveNotes(self) -> int:
        return len(self.getLoanNumbers(True))

    def getTotalPendingListings(self) -> int:
        return len(self.pending_listings)

    def getMTDPaymentsDeposited(self) -> float:
        mtdPayments: float = 0.0
        for pmt in self.payments:
            mtdPayments += pmt.payment
        return mtdPayments

    def isLastDayOfMonth(self) -> bool:
        return check_if_last_day_of_month()

    def addPendingListing(self, listingId) -> None:
        self.pending_listings.add(listingId)

    def alreadyInvestedInListing(self, listingId) -> bool:
        if listingId in self.pending_listings or self.getPaymentForListingId(listingId) is not None:
            return True
        else:
            return False


loanStatCache = NoteStats()


## Retrieve Listing Numbers with status of:
##   1 - Current
##   2 - Charge Off (Loans in recovery status)
## 
def getActiveListingStats() -> NoteStats:
    loanStats = NoteStats()
    limit = 100
    notesURL = properbaseurl + "/v1/notes/?limit=" + str(limit)
    listings = requests.request("GET", notesURL, headers=prosperAuth.getApiHeader()).json()
    # print(json.dumps(listings, indent=2))
    total_results = listings['total_count']
    this_result_count = listings['result_count']
    # logging.debug(this_result_count)
    gatherLoanNumbers(this_result_count, listings, loanStats)

    for x in range(this_result_count, total_results, limit):
        listings = requests.request("GET", notesURL + "&offset=" + str(x), headers=prosperAuth.getApiHeader()).json()
        # print(json.dumps(listings, indent=2))
        total_results = listings['total_count']
        this_result_count = listings['result_count']
        gatherLoanNumbers(this_result_count, listings, loanStats)
    # print(this_result_count)

    return loanStats


def gatherLoanNumbers(this_result_count, listings, loanStats: NoteStats):
    for y in range(this_result_count):
        payment = None
        listingID = listings['result'][y]['listing_number']
        loanNumber = listings['result'][y]['loan_number']
        note_status = listings['result'][y]['note_status']
        origination_date = listings['result'][y]['origination_date']
        #logging.info("listing")
        if note_status == 1: ## Current
            payment = Payment(loanNumber, listingID, True)
        elif note_status == 2:  ## ChargeOff
            payment = Payment(loanNumber, listingID, False)
        elif note_status == 4:  ## Completed Loan
            today = datetime.today()
            dt = datetime.strptime(origination_date, '%Y-%m-%d')
            days = (today - dt).days
            if days < 90:
                logging.info("Skipping Loan "+str(loanNumber)+". Was paid off within "+str(days)+" days.  Anything paid withing 90 days is reinvested")
            else:
                logging.debug("Adding Completed Loan " + str(loanNumber) + ". Was paid off within " + str(
                    days) + " days.  Anything paid after 90 days is cashed out.")
                payment = Payment(loanNumber, listingID, False)
        else:
            logging.debug("listing is not longer active: " + str(listingID) + " Note Status: " + str(note_status))

        if payment is not None:
            loanStats.addPayment(payment)

def getTotalMonthToDatePaymentsReceivedForlistings(listing_numbers, loanStats: NoteStats):
    if len(listing_numbers) == 0:
        return

    firstDayOfMonth = datetime.today().replace(day=1)

    paymentsURL = properbaseurl + "/v1/loans/payments/?limit=100&loan_number=" + listing_numbers
    payments = requests.request("GET", paymentsURL, headers=prosperAuth.getPaymentsHeader()).json()
    logging.debug(json.dumps(payments, indent=2))

    this_result_count = payments['result_count']
    for x in range(this_result_count):
        ## If funds_available_date is in current month, add it to the total payments received
        if 'investor_disbursement_date' in payments['result'][x]:
            fundsAvailableDate = payments['result'][x]['investor_disbursement_date']
            dt = datetime.strptime(fundsAvailableDate[0:fundsAvailableDate.find('T')], '%Y-%m-%d')
            # logging.debug(str(firstDayOfMonth) + " Comparing to "+str(dt))
            if firstDayOfMonth.month == dt.month and firstDayOfMonth.year == dt.year:
                # if dt.month == 2 and dt.year == 2022:
                # logging.debug("This is in the current month!")
                thisLoanNumber = payments['result'][x]['loan_number']
                thisPayment = payments['result'][x]['payment_amount']
                loanStats.addPaymentForLoanNumber(thisLoanNumber, thisPayment)


## Retreive Payments for all active listings.  Payments only go back the last 90 days.
## We are only interested in payments which have posted month to date of the current month.
def getTotalMonthToDatePaymentsReceived(loanStats: NoteStats):
    loan_number_limit = 20

    logging.debug("Get payments for the following listing nummbers")
    # logging.info("Found Active Loans:"+str(loanStats.getActiveLoanNumbers()))
    # you can only get 20 loans at a time  So we need to query 20 loans,
    # get the payments the qualify and the query the next 20
    loanCount = 0
    loanNumbers = ""
    for loanNumber in loanStats.getLoanNumbers(False):
        loanCount += 1
        if len(loanNumbers) > 0: loanNumbers += ","
        loanNumbers += str(loanNumber)
        if (loanCount >= loan_number_limit):
            loanCount = 0
            getTotalMonthToDatePaymentsReceivedForlistings(loanNumbers, loanStats)
            loanNumbers = ""

    if len(loanNumbers) > 0:
        getTotalMonthToDatePaymentsReceivedForlistings(loanNumbers, loanStats)


def calcLoanStats(loanStats: NoteStats):
    loanUrl = properbaseurl + "/v1/loans/"

    ## Calculate Average Yeild for Notes
    for loanid in loanStats.getLoanNumbers(True):
        loan = requests.request("GET", loanUrl + str(loanid), headers=prosperAuth.getPaymentsHeader()).json()
        loanStats.setYeildForLoanNumber(loanid, loan['borrower_rate'])
    # loanStats.addActiveYeild(loan['borrower_rate'])

    getTotalMonthToDatePaymentsReceived(loanStats)


def check_if_last_day_of_month() -> bool:
    to_date = datetime.now()
    delta = timedelta(days=1)
    next_day = datetime.now() + delta
    if to_date.month != next_day.month:
        return True
    return False


def getLoanStats():
    global last_stat_check
    global loanStatCache

    # Lets check if we need to update our loan stats
    right_now = time.time()
    # Refresh Loan Stats every 4 hours
    if right_now - last_stat_check > (4 * 60 * 60):
        logging.info("Refeshing Loan Stats")
        try:
            loanStats = getActiveListingStats()
            calcLoanStats(loanStats)
            loanStatCache = loanStats
            logging.info(str(loanStatCache))
            last_stat_check = time.time()
        except Exception as ex:
            logging.error(traceback.format_exc())

    return loanStatCache

# print(check_if_last_day_of_month())

# loanStats = getLoanStats()
# print(loanStats.getAverageYeild())
# print(loanStats.getActiveLoanNumbers())
# print(loanStats.getMTDPaymentsDeposited())
# print("total active notes = "+str(loanStats.getTotalActiveNotes()))
