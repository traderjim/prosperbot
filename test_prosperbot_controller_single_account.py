from unittest import TestCase
from datetime import datetime

from prosperbot_controller_single_account import sleepTill

class Test(TestCase):
    def test_sleep_till1(self): ## M before 10
        now = datetime(2022, 2, 7, 8, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 6900)

    def test_sleep_till2(self): ## M before 10
        now = datetime(2022, 2, 7, 9, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 3300)

    def test_sleep_till3(self): ## M before 12
        now = datetime(2022, 2, 7, 12, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 14100)

    def test_sleep_till4(self): ## M after 3PM
        now = datetime(2022, 2, 7, 16, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 64500)

    def test_sleep_till5(self): ## F before 10AM
        now = datetime(2022, 2, 11, 8, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 6900)

    def test_sleep_till6(self): ## F befor 3PM
        now = datetime(2022, 2, 11, 12, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 14100)

    def test_sleep_till7(self): ## F after 3PM
        now = datetime(2022, 2, 11, 17, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 71700)

    def test_sleep_till8(self): ## Sat before 1PM
        now = datetime(2022, 2, 12, 9, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 14100)

    def test_sleep_till9(self): ## Sat before 1PM
        now = datetime(2022, 2, 12, 13, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 86100)

    def test_sleep_till10(self): ## Sat before 1PM
        now = datetime(2022, 2, 13, 9, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 14100)


    def test_sleep_till11(self): ## Sun after 1PM
        now = datetime(2022, 2, 13, 13, 00, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 75300)

    def test_sleep_till12(self): ## Sun after 1PM
        now = datetime(2022, 2, 13, 12, 57, 0, 0)
        tester = sleepTill(now)
        self.assertEqual(tester, 75480)