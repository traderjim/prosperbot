import logging

import prosperAuth
import send_sms
import prosperBot2
import time
from datetime import datetime
from datetime import timedelta

from decouple import config

logging.basicConfig(filename='logProsperBot.log', filemode='a',
                    format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO)

username = config('username', default='')
password = config('password', default='')
clientid = config('clientid', default='')
clientSecret = config('clientSecret', default='')

# twilio_account_sid = config('twilio_account_sid', default='')
# twilio_auth_token = config('twilio_auth_token', default='')
# twilio_from_phone = config('twilio_from_phone', default='')
# twilio_to_phone = config('twilio_to_phone', default='')

# print(twilio_to_phone, twilio_from_phone)
prosperAuth.initialize(clientid, clientSecret, False, username, password)

if __name__ == '__main__':
    prosperBot2.run()
