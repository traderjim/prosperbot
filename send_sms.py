# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client
from decouple import config

import traceback
import logging
import time
import decouple

last_keep_alive = float('0')


class Twilio:
    twilio_account_sid: str = ""
    twilio_auth_token: str = ""
    twilio_from_phone: str = ""
    twilio_to_phone: str = ""

    def __init__(self, account_sid: str = "", auth_token: str = "", from_phone: str = "", to_phone: str = ""):
        self.twilio_account_sid = config('twilio_account_sid', default='')
        self.twilio_auth_token = config('twilio_auth_token', default='')
        self.twilio_from_phone = config('twilio_from_phone', default='')
        self.twilio_to_phone = config('twilio_to_phone', default='')

        # self.twilio_account_sid = account_sid
        # self.twilio_auth_token = auth_token
        # self.twilio_from_phone = from_phone
        # self.twilio_to_phone = to_phone


twilio_auth = Twilio()


# class Twilio_Auth:
#    twilio_auth: Twilio
#    def __init__(self, twilio: Twilio):
#        self.twilio_auth = twilio


def initialize(account_sid, auth_token, from_phone, to_phone):
    global twilio_auth
    twilio_auth = Twilio(account_sid, auth_token, from_phone, to_phone)


def sendSMS(body, to_phone_number_override: str = ""):
    if twilio_auth.twilio_account_sid == '' or twilio_auth.twilio_auth_token == '' \
            or twilio_auth.twilio_from_phone == '' or twilio_auth.twilio_to_phone == '':
        logging.debug("SMS Not Setup.  Nothing to text.")
        ## Do Nothing.  Twilio is not setup
        return
    if to_phone_number_override == "":
        to_phone_number_override = twilio_auth.twilio_to_phone
    # Find your Account SID and Auth Token at twilio.com/console
    # and set the environment variables. See http://twil.io/secure
    try:
        client = Client(twilio_auth.twilio_account_sid, twilio_auth.twilio_auth_token)

        message = client.messages \
            .create(
            body=body,
            from_=twilio_auth.twilio_from_phone,
            to=to_phone_number_override
        )
        logging.debug("SMS Sent: " + body)
    except Exception as ex:
        logging.error(traceback.format_exc())


def dailyCheckin(loanStats):
    global last_keep_alive
    now = time.time()
    # If 12 hours has past since last keep alive text was sent and it's between 9AM and 10AM,
    # go ahead and send a keep alive text
    if now - last_keep_alive > (24 * 60 * 60):
        # 12 hours has passed -- now check if it's between 9AM and 10 AM
        if time.localtime().tm_hour == 9:
            msg = "Prosper Bot is still alive! (^_^) \n\n" \
                  + "I am invested in: " + str(loanStats.getTotalActiveNotes()) + " notes\n" \
                  + "Avg yield of " + str(loanStats.getAverageYeild())

            msg += "%\nMTD Pmts Rcvd:$" + '{0:.2f}'.format(loanStats.getMTDPaymentsDeposited())
            if (loanStats.isLastDayOfMonth()):
                msg += "\n\n***Please withdraw above amount today!***"
            sendSMS(msg)
            last_keep_alive = time.time()


def sendSummaryOfRun(loanStats):
    if time.localtime().tm_hour < 12:
        morning_afternoon = "morning"
    else:
        morning_afternoon = "afternoon"

    msg = "Prosper Bot Investor Status (^_^) \n\n" \
          + "I am invested in " + str(loanStats.getTotalActiveNotes()) + " notes\n" \
          + "Avg yield of " + str(loanStats.getAverageYeild())

    msg += "%\nMTD Pmts Rcvd $" + '{0:.2f}'.format(loanStats.getMTDPaymentsDeposited())
    pendingOrders = loanStats.getTotalPendingListings()
    if pendingOrders > 0:
        msg += "\n\nI have invested in " + str(pendingOrders) + " new notes this " + morning_afternoon
    if loanStats.isLastDayOfMonth():
        msg += "\n\n***Please withdraw $" + '{0:.2f}'.format(loanStats.getMTDPaymentsDeposited()) + " today!***"
    sendSMS(msg)

# checkKeepAlive()


# sendSMS("Test Me Out!")
