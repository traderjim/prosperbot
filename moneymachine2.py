



def getInputParams(baseLOC, montlyContribution, years):
	baseLOC = input("Enter base loan amount: ")
	montlyContribution = input("Enter base Monthly Contribution:")
	years = input("Enter number of years to simulate:")


def addRatio(ratio, finalMonthlyIncome, totalContributions):
	print("hello")

def printMonthlyDetails(x, baseLOC, monthlyIncome, balance, montlyContribution, numActiveLoans, monthlyInterest, monthlyPrincipal, flip, monthlyTakeout, percentIncrease):
	if x == 0:
		print("month\tLOC\t\tcontribution\tmonthly Income\tbalance\t\tinterest\t\tprincipal")
	print(str(x).rjust(3), end='\t')
	print(str('${:,.2f}'.format(baseLOC)).rjust(10),end ='\t')
	print('${:,.2f}'.format(montlyContribution),end ='\t')
	print(str('${:,.2f}'.format(monthlyIncome)).rjust(10),end ='\t')
	print(str('${:,.2f}'.format(balance)).rjust(10),end ='\t')
	print(str('${:,.2f}'.format(monthlyInterest)).rjust(10),end ='\t')
	print(str('${:,.2f}'.format(monthlyPrincipal)).rjust(10),end ='\t')
	print(str('${:,.2f}'.format(monthlyTakeout)).rjust(10),end ='\t')
	print(str('%{:,.2f}'.format(percentIncrease)).rjust(10),end ='\t')
	print("Loans: " + str(numActiveLoans),end ='\t')
	print(flip)


def calcScenario(loanAmt, baseLOC, montlyContribution, years):

	takeoutBalance = 15000
	maxTakeout = 6000
	loanRate = .114
	loanMonths = 36
	baseLOC = int(baseLOC)
	beginningLOC = baseLOC
	montlyContribution = int(montlyContribution)
	begninningMonthly = montlyContribution
	balance = baseLOC
	years = int(years)
	totalBalance = baseLOC

	#print(baseLOC)
	#print(montlyContribution)

	Loans = []
	monthsBetweenFlips = 0 
	totalContributions = 0
	finalMonthlyIncome = 0
	monthlyTakeout = 0
	totalMonthlyTakeout = 0
	flip = ""
	lastMonthlyIncome = 0

	for x in range(0, (years*12)):
		flip = ""
		
		percentIncrease = 0.0
		monthsBetweenFlips += 1
		## Create New Loans if we need to once balance is > than the LOC
		if balance >= loanAmt:
			#if monthsBetweenFlips <= 3 and x > 0:
			#	baseLOC += 10000
			#	balance += baseLOC
			flip = "Flip"
			monthsBetweenFlips = 0
			while(balance >= loanAmt):
				# Create a new loan
				# print("New Loan Established")
				#if(monthsBetweenLoans <= 3):
					#baseLOC += 5000
					#if montlyContribution > 0: montlyContribution -= 500

				#monthsBetweenLoans = 0
				balance -= loanAmt
				#loanPayment = round(((loanRate/12) * (1/(1-(1+loanRate/12)**(-loanMonths)))*loanAmt),2) 
				loanPayment = list(amortization_schedule(loanAmt, loanRate, loanMonths))
				thisLoan = [baseLOC, loanPayment, 0]
				Loans.append(thisLoan)
			
		#monthsBetweenLoans += 1

		balance += montlyContribution
		totalContributions += montlyContribution
		balance = round(balance , 2)
		monthlyIncome = 0
		numActiveLoans = 0
		monthlyInterest = 0
		monthlyPrincipal = 0

		# Collect this months payment
		for l in Loans:
			if l[2] < loanMonths:
				loanPayment = l[1][l[2]][1]
				loanInterest = l[1][l[2]][2]
				loanPrincipal = l[1][l[2]][3]
				balance += loanPayment
				l[2] += 1
				numActiveLoans += 1
				monthlyIncome += loanPayment
				monthlyInterest += loanInterest
				monthlyPrincipal += loanPrincipal
			#print('${:,.2f}'.format(l[1]), end ='\t')

		totalBalance += monthlyInterest
		totalBalance += montlyContribution
		finalMonthlyIncome = monthlyIncome
		if lastMonthlyIncome > 0:
			percentIncrease = (finalMonthlyIncome - lastMonthlyIncome) / lastMonthlyIncome * 100
		lastMonthlyIncome = finalMonthlyIncome

		#if finalMonthlyIncome > takeoutBalance:
		#	montlyContribution = 0
		#	monthlyTakeout = finalMonthlyIncome - takeoutBalance
		#	if monthlyTakeout > maxTakeout:
		#		monthlyTakeout = maxTakeout
		#	balance -= (monthlyTakeout)
		#	totalMonthlyTakeout += monthlyTakeout

		
		printMonthlyDetails(x, totalBalance, monthlyIncome, balance, montlyContribution, numActiveLoans, monthlyInterest, monthlyPrincipal, flip, monthlyTakeout, percentIncrease)

		
		
		#print("")
	ratio = round(finalMonthlyIncome / totalContributions * 100,2)
	ratioArr.append(ratio)
	print("--------------------------------------------------")

	print("Beginning LOC:", str('${:,.2f}'.format(beginningLOC)).rjust(10), " ending: ", str('${:,.2f}'.format(baseLOC)).rjust(10))
	print("Beginning Contribution", str('${:,.2f}'.format(begninningMonthly)).rjust(6), " ending:", str('${:,.2f}'.format(montlyContribution)).rjust(6))
	print("years: ", years)
	print("total Contribution: ", '${:,.2f}'.format(totalContributions))
	print("final Monthly Income: ", '${:,.2f}'.format(finalMonthlyIncome))
	print("total Takeout: ", '${:,.2f}'.format(totalMonthlyTakeout))
	
	print("numActiveLoans: ",numActiveLoans)
	print("Contributions to Income Ratio: ", '{:,.4f}%'.format(ratio))

	#print("--------------------------------------------------")

	#print(Loans)


def calculate_amortization_amount(principal, interest_rate, period):
    x = (1 + interest_rate) ** period
    #return principal * (interest_rate * x) / (x - 1)
    return ((interest_rate/12) * (1/(1-(1+interest_rate/12)**(-period)))*principal)

#(1, 0.8303577453212792, 0.25, 0.5803577453212792, 24.41964225467872)
#(2, 0.8303577453212792, 0.2441964225467872, 0.586161322774492, 23.83348093190423)
def amortization_schedule(principal, interest_rate, period):
    amortization_amount = calculate_amortization_amount(principal, interest_rate, period)
    number = 1
    balance = principal
    while number <= period:
        interest = balance * interest_rate / 12
        principal = amortization_amount - interest
        balance = balance - principal
        yield number, amortization_amount, interest, principal, balance if balance > 0 else 0
        number += 1
        #print( number, amortization_amount, interest, principal, balance)


#schedule = list(amortization_schedule(25, .12, 36))
#for item in schedule:
#	print(item)
#quit()


LOC = [10000] #[5000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000]
MonthContribution =[500, 1000, 1500, 2000, 2500, 3000]
yearArr = [10] # [3, 4, 5, 6, 7, 8, 9, 10]
ratioArr = []

for x in yearArr:
	for y in MonthContribution:
		for z in LOC:
			calcScenario(25, z, y, x)
			#print(finalMonthlyIncome)

print(ratioArr)


## When can I take out the monthly contribution and still grow?
## What is the rate of growth?  Month over Month growth of monthly income
## What does the rate of growth need to be to take money out of the system and sustain the system

