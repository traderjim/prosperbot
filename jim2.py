class Solution:
    # @param A : string
    # @return an integer
    paths = 0

    def numDecodings(self, A):
        ## Message "293193849920199483"
        ## Break this into an array of chars list(message)

        lst = list(A)
        outputs = 0
        self.findPath(lst)
        return self.paths

    def findPath(self, lst):
        self.paths += 1
        for x in range(len(lst)):
            value = int(lst[x])
            if x + 1 < len(lst):
                value2 = int(lst[x] + lst[x + 1])
                if value2 >= 11 and value2 <= 26:
                    #self.findPath(lst[x + 1:])
                    self.paths += 1


s = Solution()
s.numDecodings(
    "5163490394499093221199401898020270545859326357520618953580237168826696965537789565062429676962877038781708385575876312877941367557410101383684194057405018861234394660905712238428675120866930196204792703765204322329401298924190")
print(s.paths)