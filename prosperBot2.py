import prosperAuth
import send_sms
import prosperStats

import requests
import json
import time
import logging
import traceback
import sys
from datetime import datetime

logging.basicConfig(filename='logProsperBot.log', filemode='a',
                    format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO)

## Critical Variables -- Distribution must add up to 100%
investDist = {'AA': 0.03, 'A': 0.47, 'B': 0.48, 'C': 0.02}
## how much do I invest into each loan
maxInvestmentAmt = 25
## How long to I pause before I check to see if any loans are avialable
sleepytime = 40
# Keep a list of of listings ids that we have invested in to make sure we don't double up.
# It seems that there is an issue with prosper that it will sometimes return a note that you just
# invested in again and the system here will invest in the same one twice because of that.
# Keep the listings for just a short time and make sure they don't come back.

properbaseurl = "https://api.prosper.com"


def getAccountInfo():
    accountsURL = properbaseurl + "/v1/accounts/prosper/"
    response = requests.request("GET", accountsURL, headers=prosperAuth.getApiHeader()).json()
    logging.debug(json.dumps(response, indent=2))
    return response


def getAvailableListings(loanTypesToInvestIn):
    # Should only pull note types for which we have room to invest in based on % distribution
    url = properbaseurl + "/listingsvc/v2/listings/?biddable=true&invested=false&limit=500&" \
                          "exclude_co_borrower_application=true&prosper_rating=" \
          + loanTypesToInvestIn + "&dti_wprosper_loan_min=0&" \
                                  "dti_wprosper_loan_max=45&" \
                                  "employment_status_description=employed&" \
                                  "listing_term=36&" \
                                  "sort_by=verification_stage desc"
    response = requests.request("GET", url, headers=prosperAuth.getApiHeader()).json()
    logging.debug(json.dumps(response, indent=2))
    return response


def getInvestedListings():
    url = properbaseurl + "/listingsvc/v2/listings/?invested=true&limit=500"
    response = requests.request("GET", url, headers=prosperAuth.getApiHeader()).json()
    logging.debug(json.dumps(response, indent=2))
    return response


def submitOrders(payload):
    endpoint = properbaseurl + "/v1/orders/"
    logging.debug("Submitting Orders: " + endpoint)
    logging.debug("header: " + str(prosperAuth.getPostHeader()))
    response = requests.post(url=endpoint, json=payload, headers=prosperAuth.getPostHeader())
    logging.info(json.dumps(response.json(), indent=2))
    ##prosperAuth.sendemail(json.dumps(response.json(), indent=2))

    return response


## This function will determine which loan types have room available in the distribution to select.
## The return types will be used to select from prosper only the loan types we can invest in.
def getLoanTypesToSelect(totalAvailableToInvestByType):
    loansTypesToInvestIn = ""
    for k in totalAvailableToInvestByType.keys():
        if (totalAvailableToInvestByType[k] > 0):
            if len(loansTypesToInvestIn) > 0: loansTypesToInvestIn = loansTypesToInvestIn + ","
            loansTypesToInvestIn = loansTypesToInvestIn + k
    logging.debug(" Only get Types: " + loansTypesToInvestIn)
    return loansTypesToInvestIn


def calculateDistribution(grandTotal, totalByType):
    ## Now we need to calculate which types I can invest into based upon the requested distribution
    ## Cash Available = 400
    ## GrandTotal = Pending + Invested + Available Cash = 16050
    ## 	Dist	Invested	Pending	Total	% Distribution	New Distribution	Available Per Type
    ##	30.00%	AA	4750	0		4750		30.35%		29.60%				0.40%
    ##	30.00%	A	4525	0		4525		28.91%		28.19%				1.81%
    ## 	20.00%	B	3200	0		3200		20.45%		19.94%				0.06%
    ##	20.00%	C	3175	0		3175		20.29%		19.78%				0.22%
    ##	100.00%		15650	0		15650		100.00%		97.51%
    ##
    ## New Distribution = 	TypeTotal / Grand Total
    ## Available By Type = Dist - New Distribution
    ##
    ## If Avialable Per Type is Positive, you can invest into that note type.

    totalAvailableToInvestByType = {}
    for k, targetDist in investDist.items():
        percentAvailable = targetDist - (totalByType[k] / grandTotal)
        totalAvailableToInvestByType[k] = percentAvailable

    logging.info("Total Available To Invest: " + str(totalAvailableToInvestByType))
    return totalAvailableToInvestByType


#def alreadyInvestedInListing(listingId):
#    if not prosperStats.alreadyInvestedInListing(listingId):
#        prosperStats.addPendingListing(listingId)
##        return False
 #   else:
 #       return True


## Returns True if there is no cash to invest, False if there is cash to invest and should keep checking
##
def processOrders(loanStats) -> bool:
    accountInfo = getAccountInfo()

    ## Check for an available cash balance
    availableCash = accountInfo['available_cash_balance']
    availableCash -= loanStats.getMTDPaymentsDeposited()
    logging.info("availableCash = " + str(accountInfo['available_cash_balance']) + " - " + str(
        loanStats.getMTDPaymentsDeposited()) + " = " + str(availableCash))
    if (availableCash < maxInvestmentAmt):
        logging.debug("Not enough cash available to invest")
        ## Nothing to invest with :(
        return False

    ## Add Investment totals by type (And Total Them Up)
    totalInvested = 0.0
    totalByType = {}
    for noteType, value in accountInfo['invested_notes'].items():
        # print(noteType, value, totalInvested)
        if noteType not in totalByType:
            totalByType[noteType] = value
        else:
            totalByType[noteType] += value
        totalInvested += value

    ## Add Total Pending Investments by Type (And Total them up)
    totalPendingInvestments = 0.0
    for noteType, value in accountInfo['pending_bids'].items():
        if noteType not in totalByType:
            totalByType[noteType] = value
        else:
            totalByType[noteType] += value
        totalPendingInvestments += value

    grandTotal = totalInvested + totalPendingInvestments + availableCash
    logging.info("Total by type: " + str(totalByType) + " Grand Total: " + str(grandTotal))

    totalAvailableToInvestByType = calculateDistribution(grandTotal, totalByType)
    ## for efficiency, only select loan types we have room to invest in
    loanTypesToInvestIn = getLoanTypesToSelect(totalAvailableToInvestByType)
    if loanTypesToInvestIn == "":
        logging.critical("No loan types available to invest in -- THis should never happen!")
        return True

    ## Now we are going to look at available listings and see if we can invest in anything!
    availableListings = getAvailableListings(loanTypesToInvestIn)
    listing_count = availableListings['result_count']
    if (listing_count <= 0):
        #########################
        ### Let's kill the processing here.... nothing to report
        return True

    logging.info(accountInfo)
    logging.info("Retreived Available Listings: " + str(availableListings['result_count']))
    logging.debug(json.dumps(availableListings, indent=2))
    orderSubmission = {'bid_requests': []}
    totalOrdersToSubmit = 0
    for i in range(availableListings['result_count']):
        if totalOrdersToSubmit >= 80 or availableCash < maxInvestmentAmt:
            break
        ratingType = availableListings['result'][i]['prosper_rating']
        listingId = availableListings['result'][i]['listing_number']
        if loanStats.alreadyInvestedInListing(listingId):
            continue
        if (ratingType in totalAvailableToInvestByType):
            if (totalAvailableToInvestByType[ratingType] > 0):
                ## Create Investment Order
                thisOrder = {'listing_id': listingId, "bid_amount": maxInvestmentAmt}
                orderSubmission['bid_requests'].append(thisOrder)
                totalOrdersToSubmit += 1
                availableCash -= maxInvestmentAmt
                loanStats.addPendingListing(listingId)

                ## Now Recalibrate
                totalByType[ratingType] += maxInvestmentAmt
                totalAvailableToInvestByType = calculateDistribution(grandTotal, totalByType)

    submissionPayload = json.dumps(orderSubmission, indent=2)
    logging.info("Orders To Submit: " + submissionPayload)

    if (totalOrdersToSubmit > 0):
        ## Let's submit them!!
        submitOrders(orderSubmission)
        #send_sms.sendSMS('Just submitted ' + str(totalOrdersToSubmit) + ' New Prosper Orders! :-)')

    return True


def configCheck():
    ## Check Configuration!
    checkInvest = 0
    for k, v in investDist.items():
        checkInvest += v
    if checkInvest != 1.0:
        logging.critical(
            "EXITING PROGRAM!  Investment Distrubtion must equal 1.0.  It totals up to " + str(checkInvest))
        print("EXITING PROGRAM!  Investment Distribution must equal 1.0.  It totals up to " + str(checkInvest))
        sys.exit()


def run():
    startExecution = datetime.now()
    configCheck()

    ## Execute Main Process
    loop_count = 0
    massiveExceptionCount = 0
    cashAvailable = True
    ## Only run for 14 minutes
    while (datetime.now() - startExecution).total_seconds() < 14 * 60 and cashAvailable:
        loop_count += 1
        try:
            loanStats = prosperStats.getLoanStats()
            cashAvailable = processOrders(loanStats)
        except Exception as e:
            massiveExceptionCount += 1
            logging.error("Have encountered " + str(massiveExceptionCount) + " Major Exceptions!!!!")
            if massiveExceptionCount < 100:
                logging.error(traceback.format_exc())

        logging.info("Going to sleep for " + str(sleepytime) + " seconds (" + str(loop_count) + ")")
        time.sleep(sleepytime)

    send_sms.sendSummaryOfRun(loanStats)

# print(availableListings)


# print(prosperInvestedNotes.totalAvailableToInvest)
# print(investedListings)
