from datetime import datetime
import requests
import logging

properbaseurl = "https://api.prosper.com"

accountsURL = properbaseurl+"/v1/accounts/prosper/"

authurl = properbaseurl + "/v1/security/oauth/token"
authheader = { 'accept': "application/json", 'content-type': "application/x-www-form-urlencoded" }
apiheader = {}
postheader = {}
paymentsheader = {}
expireSeconds = -1
lastChecked = datetime.now()

accessToken = ""
refreshToken = ""

auth_keys = None

class AuthKeys:
	prosper_username: str = ""
	prosper_password: str = ""
	prosper_clientid: str = ""
	prosper_client_secret: str = ""
	is_third_party: bool = False

	def __init__(self, client_id:str, client_secret:str, is_third_party:bool, username:str="", password:str =""):
		self.prosper_clientid = client_id
		self.prosper_client_secret = client_secret
		self.is_third_party = is_third_party
		self.prosper_username = username
		self.prosper_password = password

def initialize( client_id:str, client_secret:str, is_third_party:bool, username:str="", password:str =""):
	global auth_keys
	auth_keys = AuthKeys(client_id, client_secret, is_third_party, username, password)

def __tokenRequest():
	global accessToken
	global refreshToken
	global apiheader
	global expireSeconds
	global lastChecked
	global postheader
	global paymentsheader
	if auth_keys.is_third_party:
		payload = "grant_type=authorization_key&client_id=" + auth_keys.prosper_clientid + \
				  "&client_secret=" + auth_keys.prosper_clientid
	else:
		payload = "grant_type=password&username="+auth_keys.prosper_username+"&password="+auth_keys.prosper_password\
				+"&client_id="+auth_keys.prosper_clientid+"&client_secret="+auth_keys.prosper_client_secret

	logging.debug(payload)
	response = requests.request("POST", authurl, data=payload, headers=authheader).json()
	logging.debug(response)
	accessToken = response['access_token']
	refreshToken = response['refresh_token']
	expireSeconds = response['expires_in']
	lastChecked = datetime.now()
	apiheader = { 'accept': "application/json", 'content-type': "application/x-www-form-urlencoded", 'Authorization' : "bearer "+accessToken, 'timezone': "America/Denver" }
	postheader = { 'Accept': "application/json", 'Content-Type': "application/json", 'Authorization' : "bearer "+accessToken}
	paymentsheader = { 'Accept': "application/json", 'Authorization' : "bearer "+accessToken, 'timezone': "America/Denver"}
	logging.debug(response)
	logging.debug(accessToken)


def __tokenRefresh():
	payload = "grant_type=refresh_token&client_id="+auth_keys.prosper_clientid+"&client_secret="+auth_keys.prosper_client_secret\
			  +"&refresh_token="+refreshToken
	response = requests.request("POST", authurl, data=payload, headers=authheader)
	logging.info("Refeshing Access Token")
	logging.debug(response.text)


def getApiHeader():
	nowdt = datetime.now()
	seconds = nowdt - lastChecked
	if(seconds.seconds > expireSeconds ):
		logging.info("Requesting New Token")
		__tokenRequest()
	return apiheader

def getPostHeader():
	getApiHeader()
	return postheader

def getPaymentsHeader():
	getApiHeader()
	return paymentsheader

